
from flask import Flask, request, json
from cassandra.cluster import Cluster
from utilidades.funciones import leer_fichero

query_select = "SELECT * FROM contadores"
query_insert = "INSERT INTO contadores (idContador, latitud, longitud, zona, modelo, mediciones) VALUES (%s, %s, %s, %s, %s, %s)"
query_update = "UPDATE contadores SET latitud = %s, longitud = %s, zona = %s, modelo = %s, mediciones += %s WHERE idContador = %s"
query_delete = "DELETE FROM contadores WHERE idContador = %s"

# Establecer la conexión
cluster = Cluster(['localhost'])
session = cluster.connect('ca_bd')

app = Flask(__name__)


@app.route('/enviar/<string:endpoint>', methods=["POST"])  # Insert o Update
def enviar(endpoint):
    """Si el registro existe en Cassandra, lo actualiza, en caso contrario, lo crea"""

    if endpoint == 'contador':
        params = request.get_json()

        idContador = params['idContador']
        latitud = params['latitud']
        longitud = params['longitud']
        zona = params['zona']
        modelo = params['modelo']

        tiempo_long = params['mediciones']['fecha']
        caudal = params['mediciones']['caudal']

        mediciones = {tiempo_long: caudal}

        # Verifico si el registro ya existe
        registro_presente = session.execute("SELECT idContador FROM contadores WHERE idContador = %s", (idContador,))
        if registro_presente:
            # Si el registro existe, realiza una actualización
            session.execute(query_update, (latitud, longitud, zona, modelo, mediciones, idContador))
            return "Registro actualizado correctamente"
        else:
            # En caso contrario, realiza una inserción
            session.execute(query_insert, (idContador, latitud, longitud, zona, modelo, mediciones))
            return "Registro enviado correctamente"

    return "Ha ocurrido un error"


@app.route('/contadores', methods=['GET'])
def get_contadores():
    """Realiza un select contra la base de datos Cassandra de Docker"""

    result = session.execute(query_select)
    dict_r = {}

    for row in result:
        print(row.id, row.latitud, row.longitud, row.zona, row.modelo, row.mediciones, "\n\n")
        dict_r.update({str(row.id): str(row.latitud) + str(row.longitud) + str(row.zona) + row.modelo + str(row.mediciones)})

    return dict_r


@app.route('/eliminar/<string:endpoint>/<int:id>', methods=["DELETE"])
def eliminar(endpoint, id):
    """Elimina un registro según el id que le llegue"""

    if endpoint == 'contador':

        session.execute(query_delete, (id,))
        return "Registro eliminado correctamente"

    return "Ha ocurrido un error"


if __name__ == '__main__':
    dict_datos = leer_fichero("./recursos/data.txt")
    ip = dict_datos.get("ip_local")
    puerto = dict_datos.get("puerto_flask")

    app.run(host=ip, port=puerto)
