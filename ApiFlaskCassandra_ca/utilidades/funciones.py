
def leer_fichero(ruta_fichero):
    """Lee el fichero que haya en la ruta que le llegue como parámetro y devuelve un diccionario con su contenido"""

    r_dict = {}
    # Obtengo línea a línea el contenido del archivo y lo voy añadiendo al diccionario
    with open(ruta_fichero, "r") as archivo:
        for linea in archivo:
            linea_list = linea.split(" ")
            r_dict.update({linea_list[0]: linea_list[1].replace("\n", "")})

    return r_dict
